var scheduletype = ''
var startTime = '';
var interval = '';
var intervalPause = '';

var isPriority = false;
var isStarted = false;
var elapsed = 0;

var isProcessing = false;
var algorithm ='';
var accumBurst = 0;
var currentProcessing = 0;
var lastcurrentProcessing = 0;



var isBlank=false;
var isBlankRemoved = 0;
var removeInterval = '';

var added = 0;
var processed = 0;
var ganttCount = 0;
var ganttRow = 0;

var accumulativeWT =0;
var accumulativeATAT =0;
var atatOffset = 0;
var atatOffCount = 0;

var lastPercentBurst = 0;
var processingStart = 0;
var processingBurst = 0;




var prioritychecker = new Array();

var processArray = new Array();


function generateColor(numOfSteps, step) {
    // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
    // Adam Cole, 2011-Sept-14
    // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
    var r, g, b;
    var h = step / numOfSteps;
    var i = ~~(h * 6);
    var f = h * 6 - i;
    var q = 1 - f;
    switch(i % 6){
        case 0: r = 1; g = f; b = 0; break;
        case 1: r = q; g = 1; b = 0; break;
        case 2: r = 0; g = 1; b = f; break;
        case 3: r = 0; g = q; b = 1; break;
        case 4: r = f; g = 0; b = 1; break;
        case 5: r = 1; g = 0; b = q; break;
    }
    var c = "#" + ("00" + (~ ~(r * 255)).toString(16)).slice(-2) + ("00" + (~ ~(g * 255)).toString(16)).slice(-2) + ("00" + (~ ~(b * 255)).toString(16)).slice(-2);
    return (c);
}


function selectedScheduling(value){
	scheduletype = value;
	var alg = '';
	algorithm = value;

	document.getElementById('presimulationDivision').className = 'noselect';
	document.getElementById('simulationDivision').className = '';

	
	
	if(value!='p'){
	    var elements = document.getElementsByClassName('cellPriority');
		while(elements.length > 0){
        elements[0].parentNode.removeChild(elements[0]);
		}
	}
	else{
		isPriority = true;
	}
	
	switch(value) {
    case 'fcfs':
        alg = 'First Come First Serve';
        break;
    case 'sjf':
        alg = 'Shortest Job First';
        break;
    default:
         alg = 'Priority';
	}
	document.getElementById('algorithm').innerHTML = 'Algorithm: '+alg;
	document.getElementById('elapsedcontainer').className = '';
	document.getElementById('statuscontainer').className = '';
}


function checkBurst(value){
	value = Math.ceil(value);
	if(value>0 && value<31 && value!=''){
		
	}
	else{
	if(value<1){
		document.getElementById('addBurstNumber').value = '1';
	}
	else if(value>30){
		document.getElementById('addBurstNumber').value = '30';
	}
	else{
		document.getElementById('addBurstNumber').value = '1';
	}
	}	
}


function checkArrival(value){
	value = Math.ceil(value);
	if(value>-1 && value<31 && value!=''){
		
	}
	else{
	if(value<0){
		document.getElementById('addArrivalNumber').value = '0';
	}
	else if(value>30){
		document.getElementById('addArrivalNumber').value = '30';
	}
	else{
		document.getElementById('addArrivalNumber').value = '0';
	}
	}	
}

function checkSize(value){
	value = Math.ceil(value);
	if(value>9 && value<101  && value!=''){
		
	}
	else{
	if(value<10){
		document.getElementById('addSizeNumber').value = '10';
	}
	else if(value>100){
		document.getElementById('addSizeNumber').value = '100';
	}
	else{
		document.getElementById('addSizeNumber').value = '50';
	}
	}		
}

function checkPriority(value){
	value = Math.ceil(value);
	if(value>0 && value<1000  && value!=''){
		
	}
	else{
	if(value<1){
		document.getElementById('addPriorityNumber').value = '1';
	}
	else if(value>999){
		document.getElementById('addPriorityNumber').value = '999';
	}
	else{
		document.getElementById('addPriorityNumber').value = '10';
	}
	}		
}


function startSimulation(){
	document.getElementById('startsimulbutton').value = 'Pause';
	document.getElementById('startsimulbutton').onclick = function() { 
            pause(); 
        };
	
	document.getElementById('rerunsimulbutton').className = 'buttonstyled noselect';
	document.getElementById('statuscontainer').innerHTML = 'Status: Processing';
	document.getElementById('resultDivision').className = '';
	insertToProcess();
	isProcessing=true;
	isStarted= true;
	startTime = Date.now();
	interval = setInterval(function() {timer()}, 100);	
	
}

function pause(){
	clearInterval(interval);
	intervalPause = setInterval(function() {startTime = startTime+100;}, 100);	
	
	document.getElementById('startsimulbutton').value = 'Resume';
	document.getElementById('startsimulbutton').onclick = function() { 
            unpause(); 
        };
	document.getElementById('statuscontainer').innerHTML = 'Status: Paused';
	document.getElementById('processName').innerHTML = document.getElementById('processName').innerHTML = '<span id = "bulletMark"  class = "waitingBullet">&#11044;</span>Process '+currentProcessing+' (Paused)';
}

function unpause(){
	clearInterval(intervalPause);
	startTime = startTime+100;
	interval = setInterval(function() {timer()}, 100);	
	
	document.getElementById('startsimulbutton').value = 'Pause';	
	document.getElementById('startsimulbutton').onclick = function() { 
            pause(); 
        };	
	document.getElementById('statuscontainer').innerHTML = 'Status: Processing';
	document.getElementById('processName').innerHTML = document.getElementById('processName').innerHTML = '<span id = "bulletMark"  class = "processingBullet">&#11044;</span>Process '+currentProcessing;
}

function timer(){
    var elapsedTime = Date.now() - startTime;
	elapsedTime = (Math.ceil(elapsedTime/ 100) * 100);
	var curTime = (parseInt(elapsedTime) / 1000).toFixed(1);
	var ganttTableDetails = document.getElementById('ganttTableDetails');
    document.getElementById("timer").innerHTML = curTime;	
	elapsed = curTime;
	
	

	
	
	if(isProcessing){
	var curPercentBurst = Math.ceil(((parseFloat(curTime)-parseFloat(processingStart))/processingBurst)*100);
	document.getElementById("loaderPercent").innerHTML = curPercentBurst+'%';


	if(lastPercentBurst>curPercentBurst){
		$('#loaderbar').css('transition-duration','0s');
		$('#loaderbar').css('-webkit-transition-duration','0s');
	}
	else{
		$('#loaderbar').css('transition-duration','0.1s');
		$('#loaderbar').css('-webkit-transition-duration','0.1s');
	}
	
	
	lastPercentBurst = curPercentBurst;
	$('#loaderbar').css('width',Math.ceil((200*curPercentBurst)/100)+'px');
	
	if(isBlank){
	isBlank = false;
	}
	
	if(lastcurrentProcessing==currentProcessing){
		document.getElementById('endtime'+lastcurrentProcessing).innerHTML = Math.floor(elapsed);
	}
	else{
		ganttTableDetails.innerHTML = ganttTableDetails.innerHTML+'<tr><td style = "text-align: left;"><span style = "color:'+processArray[currentProcessing-1].lastColor+';">&#11044;</span>P'+currentProcessing+'</td><td>'+Math.floor(elapsed)+'</td><td id = "endtime'+currentProcessing+'"></td></tr>';
	}
	lastcurrentProcessing=currentProcessing;

	}
	else{
		
		if(isBlank){
		document.getElementById('endtime0').innerHTML = Math.floor(elapsed);
		atatOffset++;
		
		}
		else{
			if (document.getElementById('endtime0')===null){
				
			}
			else{
				document.getElementById('endtime0').id = '';
			}
			
			ganttTableDetails.innerHTML = ganttTableDetails.innerHTML+'<tr><td style = "text-align: left;"><span style = "color:rgba(0,0,0,0.3);">&#11044;</span>Idle</td><td>'+Math.floor(elapsed)+'</td><td id = "endtime0"></td></tr>';	
			isBlank = true;
			atatOffset++;
			atatOffCount++;
			
					
			
		}
			

		
		document.getElementById("loaderPercent").innerHTML = '0%';
		$('#loaderbar').css('transition-duration','0s');
		$('#loaderbar').css('-webkit-transition-duration','0s');
		$('#loaderbar').css('width','0px');	
		$('#loaderbar').css('transition-duration','0.1s');
		$('#loaderbar').css('-webkit-transition-duration','0.1s');
	}
	
	
	if(curTime%1=='0'){
		
		$('#bulletMark').css('opacity','1');
		checkFinished();
	}
	else if(curTime%1=='0.5'){
		$('#bulletMark').css('opacity','0');
	}
	
	
	if(atatOffCount>0){
		var sign = '';
		var offsetValue = (((parseFloat(accumulativeATAT)+parseFloat(atatOffset/10))/(parseFloat(processed)+parseFloat(atatOffCount)))-(parseFloat(accumulativeATAT)/(parseFloat(processed)))).toFixed(2);
		
		if(offsetValue.charAt(0)=='-'){
			sign = ' - ';
			offsetValue = offsetValue.replace('-','');
		}
		else{
			sign = ' + ';
		}
		document.getElementById('atatoffsetcontainer').innerHTML = sign+offsetValue+'s (Idle ATAT offset)';
	}
}


function addProcess(){
	var table ='';
	var validitycheck = true;
	var validitymessage = '';
	var priorityvalue ='';
	var idextension = '';
	var currentP = parseInt(document.getElementById('addProcessId').innerHTML.replace('P',''));
	if(!isStarted && parseInt(Math.ceil(document.getElementById('addArrivalNumber').value))=='0'){
	document.getElementById('startsimulbutton').className = 'buttonstyled';
	}
	
	
	if(document.getElementById('addArrivalNumber').value == 0 && elapsed==0){
		table = document.getElementById("processingqueuetable");
		idextension = 'processing';
	}
	else{
		table = document.getElementById("arrivalqueuetable");
		idextension = 'arriving';
	}
	
	if(isPriority){
		document.getElementById('addPriorityNumber').value = parseInt(Math.ceil(document.getElementById('addPriorityNumber').value));
		priorityvalue = document.getElementById('addPriorityNumber').value;
		if(priorityvalue in prioritychecker){
			validitycheck = false;
			validitymessage = '*Priority value already exists.';
		}
		else{
			prioritychecker[priorityvalue] = 'keyed';
			validitycheck = true;
		}
		
	}
	else{
		validitycheck = true;
	}
	
	if(validitycheck){
	document.getElementById('messageAdd').innerHTML = '&nbsp;';
	var tr = document.createElement("tr");
	tr.setAttribute("id", 'tr'+idextension+":"+currentP);
	
	var tdp= document.createElement("td");
	tdp.setAttribute("id", idextension+"p:"+currentP);
	var valuetdp = document.createTextNode(document.getElementById('addProcessId').innerHTML);
	
	var tds= document.createElement("td");
	tds.setAttribute("id", idextension+"s:"+currentP);
	var valuetds = document.createTextNode(parseInt(Math.ceil(document.getElementById('addSizeNumber').value)));

	var tdbt= document.createElement("td");
	tdbt.setAttribute("id", idextension+"bt:"+currentP);
	var valuetdbt = document.createTextNode(parseInt(Math.ceil(document.getElementById('addBurstNumber').value)));

	var tdat= document.createElement("td");
	if(idextension == 'arriving'){
	tdat.setAttribute("class", "arrivalValues");
	}
	tdat.setAttribute("id", idextension+"at:"+currentP);
	var atval = '';
	if(elapsed == '0'){
		atval = parseInt(Math.ceil(document.getElementById('addArrivalNumber').value));
	}
	else{
		atval = parseInt(Math.ceil(document.getElementById('addArrivalNumber').value)+parseInt(elapsed)+parseInt(1));
	}
	var valuetdat = document.createTextNode(atval);


	document.getElementById('addSizeNumber').value = '50';
	document.getElementById('addBurstNumber').value = '1';
	document.getElementById('addArrivalNumber').value = '0';
	var curP = document.getElementById('addProcessId').innerHTML.replace("P",'');
	document.getElementById('addProcessId').innerHTML = 'P'+(parseInt(curP)+1);
	
	if(isPriority){
	var tdpr= document.createElement("td");
	tdpr.setAttribute("id", idextension+"pr:"+currentP);
	
	var valuetdpr = document.createTextNode(parseInt(Math.ceil(document.getElementById('addPriorityNumber').value)));
	tdpr.appendChild(valuetdpr);
	while(priorityvalue in prioritychecker){
		priorityvalue++;
	}
	document.getElementById('addPriorityNumber').value = priorityvalue;
	}	
	
	if(idextension == 'processing'){
		tdp.setAttribute("class","processValues");
		tdbt.setAttribute("class","burstValues");
		if(isPriority){
			tdpr.setAttribute("class","priorityValues");
		}
	}	
	
	tdp.appendChild(valuetdp);
	tds.appendChild(valuetds);
	tdbt.appendChild(valuetdbt);
	tdat.appendChild(valuetdat);
	
	
	tr.appendChild(tdp);
	tr.appendChild(tds);	
	tr.appendChild(tdbt);
	tr.appendChild(tdat);
	if(isPriority){
		tr.appendChild(tdpr);
	}
	
	
	table.appendChild(tr);
	added++;

	}
	else{
		document.getElementById('messageAdd').innerHTML = validitymessage;
	}
}


function checkFinished(){
	if(ganttCount>19){
		ganttRow++;
		document.getElementById('ganttTableContainer').innerHTML  = document.getElementById('ganttTableContainer').innerHTML + '<tr id ="ganttRow'+ganttRow+'"></tr>';
		ganttCount = 0;
	}
	
	if(isProcessing){
		accumBurst++;
	//gantt
	document.getElementById('ganttRow'+ganttRow).innerHTML = document.getElementById('ganttRow'+ganttRow).innerHTML+'<td style = "text-align: left;" valign = "top"><div style ="width: 50px;background-color:'+processArray[currentProcessing-1].lastColor+';height: 30px;"></div></td>';
	ganttCount++;
	//gantt		
	}
	else{
	document.getElementById('ganttRow'+ganttRow).innerHTML = document.getElementById('ganttRow'+ganttRow).innerHTML+'<td style = "text-align: left;" valign = "top"><div style ="width: 50px;background-color:rgba(0,0,0,0.3);height: 30px;"></div></td>';	
	ganttCount++;
	}

	//changed from ==
	if(parseInt(elapsed) >= parseInt(processingStart)+parseInt(processingBurst)){
		isProcessing = false;
		document.getElementById('curjobspan').innerHTML = 'P#';
		document.getElementById('jobtable').innerHTML = '<tr><td>#kb</td><td>Pg.#</td></tr>';
		document.getElementById('processName').innerHTML = '<span id = "bulletMark" class = "waitingBullet">&#11044;</span>Waiting for process';
		document.getElementById('btprocessingcontainer').innerHTML = '0';
		document.getElementById('stprocessingcontainer').innerHTML = '0';
		document.getElementById('atprocessingcontainer').innerHTML = '0';
		if(isPriority){
		document.getElementById('prprocessingcontainer').innerHTML = '0';
		}
		var mmcontainer = document.getElementsByClassName('free');
	
		for(var ctr = 0; ctr< mmcontainer.length;ctr++){
			mmcontainer[ctr].innerHTML = 'free';
		}
		//insert to final result table
		var finaltable = document.getElementById('finalresulttable');
		var finishedCollection = document.getElementsByClassName('finalProcessClass');
		var rowKey = 1;
		for(var ctr = 0; ctr<finishedCollection.length;ctr++){
			if(parseInt(finishedCollection[ctr].innerHTML.replace('P',''))<currentProcessing){
				rowKey++
			}
		}
		var curCount = 0;
		var tr = finaltable.insertRow(rowKey);
		var tdp = tr.insertCell(0);
		tdp.className = 'finalProcessClass';
		tdp.id = 'finalProcess:'+currentProcessing;
		var valuetdp=document.createTextNode("P"+(parseInt(currentProcessing)));
		tdp.appendChild( valuetdp );
		curCount++;
		var tds = tr.insertCell(curCount);
		var valuetds=document.createTextNode((processArray[currentProcessing-1]).lastS);
		tds.appendChild( valuetds );		

		curCount++;		
		var tdpg= tr.insertCell(curCount);
		var valuetpg=document.createTextNode((processArray[currentProcessing-1]).lastPg);
		tdpg.appendChild( valuetpg );	

		curCount++;
		var tdbt= tr.insertCell(curCount);
		var valuetbt=document.createTextNode((parseInt(processArray[currentProcessing-1].lastBt)));
		tdbt.appendChild( valuetbt );		

		curCount++;
		var tdat= tr.insertCell(curCount);
		var valuetat=document.createTextNode((parseInt(processArray[currentProcessing-1].lastAt)));
		tdat.appendChild( valuetat );	

		if(isPriority){	
		curCount++;		
		var tdpr= tr.insertCell(curCount);
		var valuetdpr = document.createTextNode(parseInt(processArray[currentProcessing-1].lastPr));	
		tdpr.appendChild(valuetdpr);
		}

		curCount++;		
		var tdwt= tr.insertCell(curCount);
		tdwt.className = 'finalWaitingClass';
		var valuetwt=document.createTextNode(((parseInt(elapsed)-parseInt(processArray[currentProcessing-1].lastAt)))-parseInt(processArray[currentProcessing-1].lastBt));
		tdwt.appendChild( valuetwt );

		curCount++;		
		var tdct= tr.insertCell(curCount);
		var valuetct=document.createTextNode(parseInt(elapsed));
		tdct.appendChild( valuetct );		

		curCount++;
		var tdtt= tr.insertCell(curCount);
		tdtt.className = 'finalTurnaroundClass';
		var valuettt=document.createTextNode((parseInt(elapsed)-parseInt(processArray[currentProcessing-1].lastAt)));
		tdtt.appendChild( valuettt );			
		
		accumulativeWT = accumulativeWT+((parseInt(elapsed)-parseInt(processArray[currentProcessing-1].lastAt)))-parseInt(processArray[currentProcessing-1].lastBt);
		accumulativeATAT= accumulativeATAT+(parseInt(elapsed)-parseInt(processArray[currentProcessing-1].lastAt))


		document.getElementById('awtcontainer').innerHTML = parseFloat(accumulativeWT/processed).toFixed(2);
		document.getElementById('atatcontainer').innerHTML = parseFloat(accumulativeATAT/processed).toFixed(2);
		
		
	
		
		//insert to final result table
		
	}
	var thereTable = parseInt(document.getElementsByClassName('arrivalValues').length) + parseInt(document.getElementsByClassName('processValues').length);
	

	if(processed==added && thereTable=='0' && parseInt(accumBurst)>=parseInt(processingBurst)){
		//marker1
		

		clearInterval(interval);

		document.getElementById('startsimulbutton').value = 'Start';	
		document.getElementById('startsimulbutton').className = 'buttonstyled noselect';	
		document.getElementById('startsimulbutton').onclick = function() { 
			startSimulation();
        };			
		
		document.getElementById('statuscontainer').innerHTML = 'Status: Finished';
		document.getElementById('processName').innerHTML = '<span id = "bulletMark" class = "stoppedBullet">&#11044;</span>No process running';
		
		
		document.getElementById("loaderPercent").innerHTML = '0%';
		$('#loaderbar').css('transition-duration','0s');
		$('#loaderbar').css('-webkit-transition-duration','0s');
		$('#loaderbar').css('width','0px');
		$('#loaderbar').css('transition-duration','0.1s');
		$('#loaderbar').css('-webkit-transition-duration','0.1s');
		document.getElementById('addProcessButton').className = 'buttonstyled noselect';
		document.getElementById('rerunsimulbutton').className = 'buttonstyled';
		
	}
	else{
	checkArriving();
	}
}



function checkArriving(){
	var arrivingchecker = new Array();
	arrivingchecker =document.getElementsByClassName('arrivalValues');
	var curcollection= new Array();
	var ctrcollection = new Array();
	var therePushed = -1;
	for(var ctr = 0; ctr<arrivingchecker.length;ctr++){
		if(parseInt(arrivingchecker[ctr].innerHTML)==elapsed){
			curcollection.push(parseInt(arrivingchecker[ctr].id.replace('arrivingat:','')));
			ctrcollection.push(ctr);
			therePushed = arrivingchecker[ctr].innerHTML;
		}
	}
	
	if(therePushed>-1){
	
	relocateArrival(curcollection,ctrcollection,therePushed);
	}
	
	if(!isProcessing && parseInt(document.getElementsByClassName('processValues').length)>0){
		insertToProcess();
	}
}


function relocateArrival(curnumber,curctr,arrival){
	for(var ctr = 0;ctr<curnumber.length;ctr++){
			var table = document.getElementById("processingqueuetable");
			
			var tr = document.createElement("tr");
			tr.setAttribute("id", 'trprocessing'+":"+curnumber[ctr]);	
	
			var tdp= document.createElement("td");
			tdp.setAttribute("id","processingp:"+curnumber[ctr]);
			tdp.setAttribute("class","processValues");
			var valuetdp = document.createTextNode('P'+curnumber[ctr]);	
			tdp.appendChild(valuetdp);
			tr.appendChild(tdp);

			var tds= document.createElement("td");
			tds.setAttribute("id","processings:"+curnumber[ctr]);
			var valuetds = document.createTextNode(parseInt(document.getElementById('arrivings:'+curnumber[ctr]).innerHTML));	
			tds.appendChild(valuetds);
			tr.appendChild(tds);

			var tdbt= document.createElement("td");
			tdbt.setAttribute("id","processingbt:"+curnumber[ctr]);
			tdbt.setAttribute("class","burstValues");
			var valuetdbt = document.createTextNode(parseInt(document.getElementById('arrivingbt:'+curnumber[ctr]).innerHTML));	
			tdbt.appendChild(valuetdbt);
			tr.appendChild(tdbt);

			var tdat= document.createElement("td");
			tdat.setAttribute("id","processingat:"+curnumber[ctr]);
			var valuetdat = document.createTextNode(parseInt(arrival));	
			tdat.appendChild(valuetdat);
			tr.appendChild(tdat);
			
			if(isPriority){	
			var tdpr= document.createElement("td");
			tdpr.setAttribute("id","processingpr:"+curnumber[ctr]);
			tdpr.setAttribute("class","priorityValues");
			var valuetdpr = document.createTextNode(parseInt(document.getElementById('arrivingpr:'+curnumber[ctr]).innerHTML));	
			tdpr.appendChild(valuetdpr);
			tr.appendChild(tdpr);
			}
			table.appendChild(tr);
			var row = document.getElementById("trarriving:"+curnumber[ctr]);
			row.parentNode.removeChild(row);
	}
}


function insertToProcess(){
	isBlankRemoved=0;
	accumBurst=0;
	processed++;
	var burstValues = new Array();
	burstValues = document.getElementsByClassName('burstValues');

	
	
	var currentProcess = '0';
	var currentBurst = '31';
	
	
	if(algorithm=='p'){
		var priorityValues = new Array();
		var currentPriority = '1000';
		priorityValues = document.getElementsByClassName('priorityValues');
		
		for(var ctr = 0; ctr<priorityValues.length;ctr++){
			if(currentProcess=='0'){
			currentProcess = priorityValues[ctr].id.replace('processingpr:','');	
			currentPriority = parseInt(priorityValues[ctr].innerHTML);
			}
			else{
				
			if(parseInt(priorityValues[ctr].innerHTML)<currentPriority){
				currentPriority = parseInt(priorityValues[ctr].innerHTML);
				currentProcess = priorityValues[ctr].id.replace('processingpr:','');
			}
			}
		}
	}
	else if(algorithm=='sjf'){
		for(var ctr = 0; ctr<burstValues.length;ctr++){
			if(currentProcess=='0'){
			currentBurst = parseInt(burstValues[ctr].innerHTML);
			currentProcess = burstValues[ctr].id.replace('processingbt:','');	
			}
			else{
			if(parseInt(burstValues[ctr].innerHTML)<currentBurst){
				currentBurst = parseInt(burstValues[ctr].innerHTML);
				currentProcess = burstValues[ctr].id.replace('processingbt:','');
			}
			else if(parseInt(burstValues[ctr].innerHTML)==currentBurst){
				if(parseInt(currentProcess)>parseInt(burstValues[ctr].id.replace('processingbt:',''))){
					currentBurst = parseInt(burstValues[ctr].innerHTML);
					currentProcess = burstValues[ctr].id.replace('processingbt:','');					
				}
			}
			}
		}		
	}
	else{
		for(var ctr = 0; ctr<burstValues.length;ctr++){
		if(currentProcess=='0'){
			currentBurst = parseInt(burstValues[ctr].innerHTML);
			currentProcess = burstValues[ctr].id.replace('processingbt:','');	
		}
		else{
			if(parseInt(currentProcess)>parseInt(burstValues[ctr].id.replace('processingbt:',''))){
				currentBurst = parseInt(burstValues[ctr].innerHTML);
				currentProcess = burstValues[ctr].id.replace('processingbt:','');		
			}			
		}	
		}		
	}
	var lastSize = parseInt(document.getElementById('processings:'+currentProcess).innerHTML);
	var lastBurst = parseInt(document.getElementById('processingbt:'+currentProcess).innerHTML);
	var lastArrival = parseInt(document.getElementById('processingat:'+currentProcess).innerHTML);
	
	
	
	var sizeDivider = lastSize;
	var jtable = document.getElementById('jobtable');
	var pagenum = 0;
	var pageval = '25';
	

	
	
	document.getElementById('processName').innerHTML = '<span id = "bulletMark"  class = "processingBullet">&#11044;</span>Process '+currentProcess;
	document.getElementById('curjobspan').innerHTML = 'P'+currentProcess;
	document.getElementById('btprocessingcontainer').innerHTML = parseInt(lastBurst);
	processingBurst = lastBurst;
	document.getElementById('stprocessingcontainer').innerHTML = parseInt(elapsed);
	processingStart = elapsed;
	document.getElementById('atprocessingcontainer').innerHTML = parseInt(lastArrival);
	jtable.innerHTML = '';
	if(isPriority){
	document.getElementById('prprocessingcontainer').innerHTML = parseInt(document.getElementById('processingpr:'+currentProcess).innerHTML);
	}
	while(sizeDivider>0){
		if(sizeDivider>24){
			pageval = '25';
		}
		else{
			pageval = sizeDivider;
		}
		sizeDivider = parseInt(sizeDivider)-25;
		jtable.innerHTML = jtable.innerHTML+"<tr><td>"+pageval+"kb</td><td>Pg. "+pagenum+"</td></tr>";
		document.getElementById('mm'+(parseInt(pagenum)+2)).innerHTML = 'P'+currentProcess+" - Pg."+pagenum;
		pagenum++;
	}
	
	if(isPriority){
	var lastPriority = parseInt(document.getElementById('processingpr:'+currentProcess).innerHTML);
		processArray[currentProcess-1] = {lastS:lastSize, lastBt:lastBurst, lastAt:lastArrival, lastPr:lastPriority, lastPg:pagenum, lastColor: generateColor((parseInt(added)+1),parseInt(currentProcessing))};
	}
	else{
		processArray[currentProcess-1] = {lastS:lastSize, lastBt:lastBurst, lastAt:lastArrival, lastPg:pagenum, lastColor: generateColor((parseInt(added)+1),parseInt(currentProcessing))};
	}
	

	currentProcessing = currentProcess;
	isProcessing = true;
	
	removeInterval = setInterval(function() {
	
	if(isBlankRemoved=='1'){
		clearInterval(removeInterval);
		isBlankRemoved=0;
	}
	else{
		removeFromProcess();
	}
	}, 100);

}

function removeFromProcess(){
	if(isProcessing && !isBlankRemoved){
	var row = document.getElementById("trprocessing:"+currentProcessing);
	row.parentNode.removeChild(row);	
	isBlankRemoved = 1;
	}	
}


function rerunSimulation(){	

	startTime = '';
	interval = '';
	intervalPause = '';
	
	isStarted = false;
	elapsed = 0;
	
	isProcessing = false;
	accumBurst = 0;
	currentProcessing = 0;
	lastcurrentProcessing = 0;

	isBlank=false;
	isBlankRemoved = 0;
	removeInterval = '';
	
	added = 0;
	processed = 0;
	ganttCount = 0;
	ganttRow = 0;

	accumulativeWT =0;
	accumulativeATAT =0;
	atatOffset = 0;
	atatOffCount = 0;	
	
	lastPercentBurst = 0;
	processingStart = 0;
	processingBurst = 0;
	
	
	if(scheduletype=='p'){
		document.getElementById('finalresulttable').innerHTML = "<tr><td>Process</td><td>Size(kb)</td><td>Pages</td><td>Burst time(s)</td><td>Arrival time(s)</td><td class = 'cellPriority'>Priority</td><td>Waiting time(s)</td><td>Completion time(s)</td><td>Turn-around time(s)</td></tr>";	
	}
	else{
		document.getElementById('finalresulttable').innerHTML = "<tr><td>Process</td><td>Size(kb)</td><td>Pages</td><td>Burst time(s)<td>Arrival time(s)</td><td>Waiting time(s)</td><td>Completion time(s)</td><td>Turn-around time(s)</td></tr>";
	}
	
	document.getElementById('ganttTableContainer').innerHTML = "<tr id ='ganttRow0'></tr>";
	document.getElementById('ganttTableDetails').innerHTML = "<tr><td>Legend</td><td>Start time(s)</td><td>End time(s)</td></tr>";
	
	document.getElementById('awtcontainer').innerHTML = '0';
	document.getElementById('atatcontainer').innerHTML = '0';
	var table ='';
	var idextension = '';

	document.getElementById('atatoffsetcontainer').innerHTML ='';
	
	
	document.getElementById('startsimulbutton').className = 'buttonstyled';
	document.getElementById('addProcessButton').className = 'buttonstyled';
	document.getElementById('rerunsimulbutton').className = 'buttonstyled noselect';
	
	document.getElementById('statuscontainer').innerHTML = 'Status: Waiting';
	
	document.getElementById('timer').innerHTML = '0';
	for(var currentpctr=0;currentpctr<processArray.length;currentpctr++){
	var curctrincremented =(parseInt(currentpctr)+parseInt(1));
	//rerunloop
	if(processArray[currentpctr].lastAt == 0){
		table = document.getElementById("processingqueuetable");
		idextension = 'processing';
	}
	else{
		table = document.getElementById("arrivalqueuetable");
		idextension = 'arriving';
	}	
	document.getElementById('messageAdd').innerHTML = '&nbsp;';
	var tr = document.createElement("tr");
	tr.setAttribute("id", 'tr'+idextension+":"+curctrincremented);
	
	var tdp= document.createElement("td");
	tdp.setAttribute("id", idextension+"p:"+curctrincremented);
	var valuetdp = document.createTextNode('P'+curctrincremented);
	
	var tds= document.createElement("td");
	tds.setAttribute("id", idextension+"s:"+curctrincremented);
	var valuetds = document.createTextNode(parseInt(processArray[currentpctr].lastS));

	var tdbt= document.createElement("td");
	tdbt.setAttribute("id", idextension+"bt:"+curctrincremented);
	var valuetdbt = document.createTextNode(parseInt(processArray[currentpctr].lastBt));

	var tdat= document.createElement("td");
	if(idextension == 'arriving'){
	tdat.setAttribute("class", "arrivalValues");
	}
	tdat.setAttribute("id", idextension+"at:"+curctrincremented);
	var atval = '';
	var valuetdat = document.createTextNode(parseInt(processArray[currentpctr].lastAt));



	if(isPriority){
	var tdpr= document.createElement("td");
	tdpr.setAttribute("id", idextension+"pr:"+curctrincremented);
	
	var valuetdpr = document.createTextNode(parseInt(processArray[currentpctr].lastPr));
	tdpr.appendChild(valuetdpr);
	}
		
	if(idextension == 'processing'){
		tdp.setAttribute("class","processValues");
		tdbt.setAttribute("class","burstValues");
		if(isPriority){
			tdpr.setAttribute("class","priorityValues");
		}
	}
	
	tdp.appendChild(valuetdp);
	tds.appendChild(valuetds);
	tdbt.appendChild(valuetdbt);
	tdat.appendChild(valuetdat);
	
	
	tr.appendChild(tdp);
	tr.appendChild(tds);	
	tr.appendChild(tdbt);
	tr.appendChild(tdat);
	if(isPriority){
		tr.appendChild(tdpr);
	}
	
	
	table.appendChild(tr);
	added++;	
	//rerunloop	
		
}
}