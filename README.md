# CPU Scheduling Simulator
Web Application for the university course Operating Systems for I.T. that
simulates CPU Scheduling Algorithms

Includes variable table result functionality and Gantt chart generator

[Give it a try!](https://yvnbunag.gitlab.io/cpu-scheduling-simulator)

![Simple Usage](documentation/simple-usage.gif)
